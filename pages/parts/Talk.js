import React from "react";
import FormMessage from "./FormMessage";

function Talk() {
  return (
    <section className="w-full home-contact">
      <div className="container mx-auto mb-10 xl:mb-0 xl:py-20 px-4">
        <h2 className="font-sans font-semibold text-3xl xl:text-7xl text-white uppercase text-center max-w-lg mx-auto">
          Talk
          <br />
          To Us
          <img src="/images/path.png" className="mt-3 lg:mt-10 mx-auto" />
        </h2>
        <p className="text-xl xl:text-2xl text-white text-center font-light mt-4 xl:mt-10">
          We'd love to hear more. Tell us a bit about
          <br className="hidden xl:block" />
          your project and we'll be in touch with you shortly
        </p>
        <div className="w-full lg:w-2/3 mx-auto mt-8">
          <FormMessage />
        </div>
      </div>
    </section>
  );
}

export default Talk;
