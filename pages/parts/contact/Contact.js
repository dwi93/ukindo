import React from "react";
import Address from "../../../components/Address";
import FormMessage from "../FormMessage";
import TitleSection from "../TitleSection";
import Zoom from "react-reveal/Zoom";

function Contact() {
  return (
    <div className="container mx-auto pb-20 px-4">
      <TitleSection
        title="our contact"
        spacing="widest"
        color="text-gray-500"
      />
      <h2 className="font-sans font-semibold text-5xl lg:text-7xl text-white uppercase">
        Talk To Us
      </h2>
      <p className="text-2xl xl:text-4xl text-white font-medium mt-5 xl:mt-10">
        We always ready to listen. Reach us here
      </p>
      <div className="block lg:flex justify-between mt-10">
        <div className="w-full lg:w-1/2 overflow-hidden">
          <Zoom>
            <img src="/images/contact-us.png" alt="contact us" />
          </Zoom>
        </div>
        <div className="w-full lg:w-1/2 lg:pl-10">
          <Address color="text-white" />
        </div>
      </div>
      <div className="text-xl xl:text-3xl text-gray-500 font-semibold text-center w-full xl:w-1/2 mx-auto my-10">
        <p>
          We'd love to hear more. Tell us a bit about yourproject and we'll be
          in touch with you shortly
        </p>
      </div>
      <div className="w-full lg:w-2/3 mx-auto mt-8">
        <FormMessage />
      </div>
    </div>
  );
}

export default Contact;
