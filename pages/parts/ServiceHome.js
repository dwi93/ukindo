import React from "react";
import BoxHoverService from "./BoxHoverService";
import TitleSection from "./TitleSection";

function ServiceHome() {
  return (
    <section className="w-full bg-service-home">
      <div className="container mx-auto py-5 xl:py-48 px-4 xl:px-20">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
          <div className="box-grid-service w-full flex flex-col justify-center px-8 xl:px-16">
            <TitleSection
              title="our service"
              color="text-gray-500"
              spacing="widest"
            />
            <h2 className="text-5xl font-sans font-bold text-indigo-800">
              What We
              <br />
              Do
            </h2>
          </div>
          <div className="box-grid-service relative w-full flex justify-center items-center bg-gray-100">
            <div className="w-1/2 text-center">
              <img
                src="/images/thumbnail.png"
                alt="thumbnail"
                className="w-3/4 mx-auto"
              />
              <div className="flex items-center mb-3">
                <span
                  className="text-white text-xs bg-cyan-400 w-7 h-6 flex items-center justify-center"
                  style={{ borderRadius: "50%" }}
                >
                  01
                </span>
                <hr className="bg-indigo-800 w-full h-1 ml-4" />
              </div>
              <span className="text-3xl font-sans text-indigo-800">
                Application
              </span>
              <span className="block lg:hidden mt-10 text-white bg-indigo-900 rounded-full shadow-lg px-4 py-2 text-sm underline font-semibold">
                Learn More
              </span>
            </div>
            <BoxHoverService
              title="Application"
              desc="We develop applications to help users accomplish certain task
                  or activities based on a set of requirements. They can be
                  built as mobile application or web-based application"
            />
          </div>
          <div className="box-grid-service relative w-full flex justify-center items-center bg-gray-100">
            <div className="w-1/2 text-center">
              <img
                src="/images/icon-web-01.png"
                alt="thumbnail"
                className="w-3/4 mx-auto"
              />
              <div className="flex items-center mb-3">
                <span
                  className="text-white text-xs bg-cyan-400 w-7 h-6 flex items-center justify-center"
                  style={{ borderRadius: "50%" }}
                >
                  02
                </span>
                <hr className="bg-indigo-800 w-full h-1 ml-4" />
              </div>
              <span className="text-3xl font-sans text-indigo-800">
                Website Design
              </span>
              <span className="block lg:hidden mt-10 text-white bg-indigo-900 rounded-full shadow-lg px-4 py-2 text-sm underline font-semibold">
                Learn More
              </span>
            </div>
            <BoxHoverService
              title="Website Design"
              desc="Information pages which can be accessed worldwide through the internet ranging from exclusive company profile to complex e-commerce system."
            />
          </div>
          <div className="box-grid-service relative w-full flex justify-center items-center bg-gray-100">
            <div className="w-1/2 text-center">
              <img
                src="/images/icon-digital-marketing-01.png"
                alt="thumbnail"
                className="w-3/4 mx-auto"
              />
              <div className="flex items-center mb-3">
                <span
                  className="text-white text-xs bg-cyan-400 w-7 h-6 flex items-center justify-center"
                  style={{ borderRadius: "50%" }}
                >
                  03
                </span>
                <hr className="bg-indigo-800 w-full h-1 ml-4" />
              </div>
              <span className="text-3xl font-sans text-indigo-800">
                Digital Branding
              </span>
              <span className="block lg:hidden mt-10 text-white bg-indigo-900 rounded-full shadow-lg px-4 py-2 text-sm underline font-semibold">
                Learn More
              </span>
            </div>

            <BoxHoverService
              title="Digital Branding"
              desc="Building brand and maximizing marketing activities through digital technology such as Search Engine Optimization and Social Media Marketing."
            />
          </div>
          <div className="box-grid-service relative w-full flex justify-center items-center bg-gray-100">
            <div className="w-1/2 text-center">
              <img
                src="/images/thumbnail.png"
                alt="thumbnail"
                className="w-3/4 mx-auto"
              />
              <div className="flex items-center mb-3">
                <span
                  className="text-white text-xs bg-cyan-400 w-7 h-6 flex items-center justify-center"
                  style={{ borderRadius: "50%" }}
                >
                  04
                </span>
                <hr className="bg-indigo-800 w-full h-1 ml-4" />
              </div>
              <span className="text-3xl font-sans text-indigo-800">
                Digital Marketing
              </span>
              <span className="block lg:hidden mt-10 text-white bg-indigo-900 rounded-full shadow-lg px-4 py-2 text-sm underline font-semibold">
                Learn More
              </span>
            </div>

            <BoxHoverService
              title="Digital Marketing"
              desc="Defining an effective digital marketing strategy for the right market to improve brand or product awareness."
            />
          </div>
          <div className="box-grid-service relative w-full flex justify-center items-center bg-gray-100">
            <div className="w-1/2 text-center">
              <img
                src="/images/icon-digital-marketing-01.png"
                alt="thumbnail"
                className="w-3/4 mx-auto"
              />
              <div className="flex items-center mb-3">
                <span
                  className="text-white text-xs bg-cyan-400 w-7 h-6 flex items-center justify-center"
                  style={{ borderRadius: "50%" }}
                >
                  05
                </span>
                <hr className="bg-indigo-800 w-full h-1 ml-4" />
              </div>
              <span className="text-3xl font-sans text-indigo-800">
                IT System Audit
              </span>
              <span className="block lg:hidden mt-10 text-white bg-indigo-900 rounded-full shadow-lg px-4 py-2 text-sm underline font-semibold">
                Learn More
              </span>
            </div>

            <BoxHoverService
              title="IT System Audit"
              desc="Evaluating and examining existing system to ensure the system has worked effectively"
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default ServiceHome;
