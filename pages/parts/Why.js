import React from "react";
import CardWhy from "./CardWhy";
import Fade from "react-reveal/Fade";

function Why() {
  return (
    <section className="w-full home-why">
      <div className="container mx-auto py-10 xl:py-20 overflow-hidden">
        <h2 className="font-sans font-semibold text-3xl xl:text-7xl text-white uppercase text-center max-w-lg mx-auto">
          why
          <br />
          choose uki
          <img src="/images/path.png" className="mt-10 mx-auto" />
        </h2>
        <div className="w-full px-5 py-5 mt-5 xl:mt-8">
          <Fade bottom>
            <div className="grid grid-cols-1 lg:grid-cols-3 gap-10">
              <CardWhy
                imgText="/images/innovative.png"
                title="Advanced & Innovative Tools"
                text="We use advanced and cutting-edge tools to ensure clients to
                  get the best products and future proof."
              />
              <CardWhy
                imgText="/images/consultation.png"
                title="Free Consultation"
                text="When clients have undefined problems related to IT, we can
                  give free consultation to help clients understand their IT
                  needs."
              />
              <CardWhy
                imgText="/images/connection.png"
                title="International Company Connection"
                text="We have a broad network locally and internationally to provide
                  a maximum service."
              />
            </div>
          </Fade>
        </div>
      </div>
    </section>
  );
}

export default Why;
