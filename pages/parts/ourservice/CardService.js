import React from "react";
import Zoom from "react-reveal/Zoom";

function CardService({ imgText, title, desc }) {
  return (
    <div className="w-5/6 py-3 lg:w-1/4 lg:h-96 overflow-hidden inline-block lg:mx-12 my-5 bg-white rounded-lg shadow-xl">
      <Zoom>
        <div className="w-1/2 h-32 align-top flex items-center overflow-hidden mx-auto mb-3">
          <img src={`/images/${imgText}`} alt={title} />
        </div>
        <div className="px-4">
          <h3 className="text-xl xl:text-2xl text-indigo-800 text-center font-semibold">
            {title}
          </h3>
          <div className="font-light text-lg xl:text-xl italic text-center mt-3 text-gray-700">
            <p>{desc}</p>
          </div>
        </div>
      </Zoom>
    </div>
  );
}

export default CardService;
