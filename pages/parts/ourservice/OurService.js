import TitleSection from "../TitleSection";
import CardService from "./CardService";

function OurService() {
  return (
    <section className="bg-main-services">
      <div className="container mx-auto py-24 xl:py-40 px-4">
        <div className="text-center">
          <TitleSection
            title="what we do"
            spacing="widest"
            color="text-gray-500"
          />
        </div>
        <h2 className="text-5xl xl:text-6xl font-sans font-bold text-white text-center">
          Our Service
        </h2>
        <div className="w-full text-center mt-5">
          <CardService
            imgText="application.png"
            title="Application"
            desc="We develop applications to help users accomplish certain task or
              activities based on a set of requirements. They can be built as
              mobile application or web-based application"
          />
          <CardService
            imgText="website-design.png"
            title="Website Design"
            desc="Information pages which can be accessed worldwide through the internet ranging from exclusive company profile to complex e-commerce system."
          />
          <CardService
            imgText="digital-branding.png"
            title="Digital Branding"
            desc="Building brand and maximizing marketing activities through digital technology such as Search Engine Optimization and Social Media Marketing."
          />
          <CardService
            imgText="digital-marketing.png"
            title="Digital Marketing Consultant"
            desc="Defining an effective digital marketing strategy for the right market to improve brand or product awareness."
          />
          <CardService
            imgText="system-audit.png"
            title="IT System Audit"
            desc="Evaluating and examining existing system to ensure the system has worked effectively"
          />
        </div>
      </div>
    </section>
  );
}

export default OurService;
