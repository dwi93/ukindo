import React from "react";
import styles from "../../styles/Accordion.module.scss";

const Accordion = ({ title, children }) => {
  const [isOpen, setOpen] = React.useState(false);
  console.log(isOpen);
  return (
    <div className={styles.accordionWrapper}>
      <div
        className={`${styles.accordionTitle} ${isOpen ? `${styles.open}` : ""} text-2xl`}
        onClick={() => setOpen(!isOpen)}
      >
        {title}
      </div>
      <div className={`${styles.accordionItem} ${!isOpen ? `${styles.collapsed}` : ""}`}>
        <div className={styles.accordionContent}>{children}</div>
      </div>
    </div>
  );
};

export default Accordion;
