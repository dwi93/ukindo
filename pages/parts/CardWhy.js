import React from "react";

function CardWhy({imgText, title, text}) {
  return (
    <div className="border-1 border-black px-8 py-8 shadow-lg rounded-2xl bg-white">
      <img
        src={imgText}
        alt={title}
        className="w-3/5 lg:h-1/2 mx-auto mb-3"
      />
      <h3 className="text-3xl text-indigo-800 font-sans font-semibold">
        {title}
      </h3>
      <article className="text-xl text-gray-700 dark:text-white mt-4">
        <p>
          {text}
        </p>
      </article>
    </div>
  );
}

export default CardWhy;
