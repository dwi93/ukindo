import React from "react";
import TitleSection from "../TitleSection";
import Zoom from "react-reveal/Zoom";

function Hero() {
  return (
    <div className="w-full block xl:h-screen xl:flex justify-between">
      <div className="w-full xl:w-1/2 bg-about-left pt-3 xl:pt-0 relative px-4 xl:px-0 mt-20 lg:mt-0">
        <div className="container w-full mx-auto xl:w-1/2 xl:ml-80 xl:mt-40">
          <TitleSection title="know us more" color="text-indigo-800" />
          <h2 className="text-3xl xl:text-6xl font-sans font-bold text-indigo-900">
            ABOUT
            <br />
            USAHA KREATIF
            <br />
            INDONESIA
          </h2>
        </div>
        <Zoom>
          <div className="relative xl:absolute w-full xl:w-9/12 xl:bottom-20 xl:-right-20 z-5 mt-5">
            <img src="/images/about.jpg" alt="about" className="w-full" />
          </div>
        </Zoom>
      </div>
      <div className="w-full xl:w-1/2 bg-about-right -mt-36 xl:-mt-0">
        <div className="container w-full xl:w-8/12 mx-auto mt-8 px-4 xl:px-0 py-10 pt-40 lg:pt-0 xl:mt-40">
          <p className="text-xl xl:text-2xl text-white font-light">
            We are an IT-based company providing one stop solutions from
            consultation to application development. Our team consists of
            passionate, creative, highly qualified young soul professionals who
            are ready to give you the best solution to achieve your goals
            through technology.
          </p>
          <div className="mt-10 xl:mt-20">
            <div className="visi-misi text-white">
              <h3 className="text-3xl xl:text-4xl relative w-max font-bold">
                Vision //
              </h3>
              <p className="text-2xl xl:text-3xl mt-10 ml-5 xl:ml-10">
                TO BE THE LEADER OF
                <br />
                FUTURE TECHNOLOGY
              </p>
            </div>
            <div className="visi-misi text-white mt-10">
              <h3 className="text-3xl xl:text-4xl relative w-max font-bold">
                Mission //
              </h3>
              <ul className="mt-10 ml-5 xl:ml-16">
                <li className="mb-3 list-decimal">
                  <span className="text-xl">
                    To build and empower our clients to achieve their goals
                  </span>
                </li>
                <li className="mb-3 list-decimal">
                  <span className="text-xl">
                    To provide complete IT solution
                  </span>
                </li>
                <li className="mb-3 list-decimal">
                  <span className="text-xl">
                    To create technology for everybody
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Hero;
