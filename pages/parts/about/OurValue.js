import React from "react";
import Zoom from "react-reveal/Zoom";

function OurValue() {
  return (
    <section className="w-full bg-gray-50 py-10 xl:py-40">
      <div className="container mx-auto px-4">
        <h2 className="text-2xl xl:text-4xl font-bold">Our Values //</h2>
        <div className="grid grid-cols-2 lg:grid-cols-5 gap-3 mt-10">
          <Zoom duration="1000">
            <div className="w-full text-center">
              <img src="/images/icon-integrity-01.png" alt="integrity" />
              <span className="text-xl xl:text-3xl tex-indigo-900 uppercase font-medium">
                integrity
              </span>
            </div>
          </Zoom>
          <Zoom duration={1500}>
            <div className="w-full text-center">
              <img src="/images/icon-trustworthy-01.png" alt="TRUSTWORTHY" />
              <span className="text-xl xl:text-3xl tex-indigo-900 uppercase font-medium">
                TRUSTWORTHY
              </span>
            </div>
          </Zoom>
          <Zoom duration={2000}>
            <div className="w-full text-center">
              <img src="/images/icon-teamwork-01.png" alt="integrity" />
              <span className="text-xl xl:text-3xl tex-indigo-900 uppercase font-medium">
                TEAMWORK
              </span>
            </div>
          </Zoom>
          <Zoom duration={2500}>
            <div className="w-full text-center">
              <img src="/images/icon-innovative-01.png" alt="innovative" />
              <span className="text-xl xl:text-3xl tex-indigo-900 uppercase font-medium">
                innovative
              </span>
            </div>
          </Zoom>
          <Zoom duration={3000}>
            <div className="w-full text-center">
              <img src="/images/icon-one-step-01.png" alt="one step ahead" />
              <span className="text-xl xl:text-3xl tex-indigo-900 uppercase font-medium">
                one step ahead
              </span>
            </div>
          </Zoom>
        </div>
      </div>
    </section>
  );
}

export default OurValue;
