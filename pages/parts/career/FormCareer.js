import React from "react";

function FormCareer({ onCancel, onShowForm }) {
  const onSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <>
      <div className="w-11/12 lg:w-3/5 px-5 py-5 lg:mx-auto mx-4 z-50 fixed top-10 lg:top-40 left-0 lg:left-1/4 bg-white rounded-md overflow-y-scroll h-5/6 lg:h-auto">
        <form onSubmit={onSubmit}>
          <div className="form-control mb-3 w-full">
            <label
              htmlFor="fullname"
              className="text-lg text-gray-500 tracking-wide"
            >
              Full Name
            </label>
            <input
              type="text"
              className="border rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent px-2 py-2 block w-full"
              id="fullname"
            />
          </div>
          <div className="block xl:flex justify-between">
            <div className="form-control mb-3 w-full xl:mr-2">
              <label
                htmlFor="dateofbirth"
                className="text-lg text-gray-500 tracking-wide"
              >
                Date Of Birth
              </label>
              <input
                type="date"
                className="border rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent px-2 py-2 block w-full"
                id="dateofbirth"
              />
            </div>
            <div className="form-control mb-3 w-full xl:ml-2">
              <label
                htmlFor="phonenumber"
                className="text-lg text-gray-500 tracking-wide"
              >
                Phone Number
              </label>
              <input
                type="tel"
                className="border rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent px-2 py-2 block w-full"
                id="phonenumber"
              />
            </div>
          </div>
          <div className="form-control mb-3">
            <label
              htmlFor="youremail"
              className="text-lg text-gray-500 tracking-wide"
            >
              Your Email
            </label>
            <input
              type="email"
              className="border rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent px-2 py-2 block w-full mr-2"
              id="youremail"
            />
          </div>
          <div className="form-control mb-3">
            <label
              htmlFor="message"
              className="text-lg text-gray-500 tracking-wide"
            >
              Message
            </label>
            <textarea
              className="border rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent px-2 py-2 block w-full mr-2"
              rows="5"
              id="message"
            ></textarea>
          </div>
          <div className="form-control mb-3">
            <label
              htmlFor="attachcv"
              className="text-lg text-gray-500 tracking-wide"
            >
              ATTACH CV HERE (MAX 5MB)
            </label>
            <input
              type="file"
              className="border rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent px-2 py-2 block w-full mr-2"
              id="attachcv"
            />
          </div>
          <button className="bg-indigo-800 text-white px-5 py-3 rounded-full mt-3 lg:mt-8  hover:bg-indigo-600 transition-all duration-200 mr-5 focus:outline-none">
            Submit
          </button>
          <button
            className="bg-red-600 text-white px-5 py-3 rounded-full mt-3 lg:mt-8  hover:bg-red-500 transition-all duration-200 focus:outline-none"
            onClick={onCancel}
          >
            Cancel
          </button>
        </form>
      </div>
      <div className="overlay bg-black opacity-50 fixed top-0 right-0 bottom-0 left-0 z-40"></div>
    </>
  );
}

export default FormCareer;
