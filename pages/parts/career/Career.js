import React, { useState } from "react";
import Accordion from "../Accordion";
import TitleSection from "../TitleSection";
import FormCareer from "./FormCareer";

function Career() {
    const [showModal, setShowModal] = useState(false)
    const showForm = () => {
        setShowModal(true)
    }
    const closeModal = () => {
        setShowModal(false)
    }
  return (
    <section className="container mx-auto py-24 xl:py-40 px-4">
      <TitleSection
        title="work with us"
        spacing="widest"
        color="text-gray-500"
      />
      <h2 className="text-4xl xl:text-6xl font-sans font-bold text-white">
        ARE YOU THE ONE
        <br />
        WE ARE LOOKING FOR?
      </h2>
      <div className="mt-10">
        <Accordion title="Full Stack Developer">
          <div className="px-8">
            <h3 className="text-xl font-semibold tracking-wide text-white uppercase">
              requirements
            </h3>
            <hr className="h-1 my-3" />
            <ul className="list-decimal pl-3 text-lg text-white leading-10">
              <li>
                At least 1 Year(s) of working experience in the related field is
                required for this position.
              </li>
              <li>
                Hold a bachelor degree in Computer Science/Information System or
                equivalent.
              </li>
              <li>
                Required Skill(s): PHP, API, JavaScript, Git, MySQL, CSS, MVC,
                UNIX.
              </li>
              <li>Required Language(s): Bahasa Indonesia & English.</li>
              <li>Experience with PHP.</li>
              <li>Experience with unix environment.</li>
              <li>Proficient with Git, Mercurial or SVN.</li>
              <li>
                Experience with relational database (Postgre SQL) design and
                development.
              </li>
              <li>Experience with building API’s, including authentication.</li>
              <li>
                Basic understanding of front-end technologies, such as
                JavaScript, HTML5, CSS3, and similar scripting languages.
              </li>
              <li>
                Familiarity with concepts of MVC, Mocking, ORM, and RESTful.
              </li>
              <li>Basic computer networking.</li>
            </ul>
            <span className="text-base mt-5 block text-white">* Surabaya, Indonesia</span>
            <button className="bg-indigo-800 text-white px-5 py-3 rounded-full mt-8  hover:bg-indigo-600 transition-all duration-200 focus:outline-none" onClick={showForm}>
              Apply Now
            </button>
          </div>
          {
              showModal ? <FormCareer onCancel={closeModal} /> : null
          }          
        </Accordion>
        <Accordion title="Front End Developer">
          <div className="px-8">
            <h3 className="text-xl font-semibold tracking-wide text-white uppercase">
              requirements
            </h3>
            <hr className="h-1 my-3" />

            <ul className="list-decimal pl-3 text-lg text-white leading-10">
              <li>
                At least 1 Year(s) of working experience in the related field is
                required for this position.
              </li>
              <li>
                Hold a bachelor degree in Computer Science/Information System or
                equivalent.
              </li>
              <li>
                Required Skill(s): PHP, API, JavaScript, Git, MySQL, CSS, MVC,
                UNIX.
              </li>
              <li>Required Language(s): Bahasa Indonesia & English.</li>
              <li>Experience with PHP.</li>
              <li>Experience with unix environment.</li>
              <li>Proficient with Git, Mercurial or SVN.</li>
              <li>
                Experience with relational database (Postgre SQL) design and
                development.
              </li>
              <li>Experience with building API’s, including authentication.</li>
              <li>
                Basic understanding of front-end technologies, such as
                JavaScript, HTML5, CSS3, and similar scripting languages.
              </li>
              <li>
                Familiarity with concepts of MVC, Mocking, ORM, and RESTful.
              </li>
              <li>Basic computer networking.</li>
            </ul>
            <span className="text-base mt-5 block text-white">* Surabaya, Indonesia</span>
            <button className="bg-indigo-800 text-white px-5 py-3 rounded-full mt-8  hover:bg-indigo-600 transition-all duration-200 focus:outline-none" onClick={showForm}>
              Apply Now
            </button>
          </div>
          {
              showModal ? <FormCareer onCancel={closeModal} /> : null
          }
        </Accordion>
      </div>
    </section>
  );
}

export default Career;
