import React from "react";

function BoxHoverService({title, desc}) {
  return (
    <div className="box-service-hover absolute w-full h-full bg-indigo-900 top-0 left-0 flex flex-col justify-center items-center px-8 text-center">
      <span className="relative text-base font-sans text-cyan-400 tracking-wider">
        {title}
      </span>
      <div className="text-white mt-8">
        <p>
          {desc}
        </p>
      </div>
    </div>
  );
}

export default BoxHoverService;
