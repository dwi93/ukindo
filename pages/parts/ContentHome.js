import PartnerHome from "./PartnerHome";
import ProductsHome from "./ProductsHome";
import Talk from "./Talk";
import Why from "./Why";

export default function ContentHome() {
  return (
    <div className="bg-content-home py-16 lg:py-0">
      <PartnerHome />
      <ProductsHome />
      <Why />
      <Talk />
    </div>
  )
}