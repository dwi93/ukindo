import React from "react";
import GridImageHome from "./GridImageHome";

function ProductsHome() {
  return (
    <section className="w-full home-product">
      <div className="container w-full mx-auto text-center px-4 xl:px-10 xl:py-20">
        <h2 className="font-sans font-semibold text-3xl xl:text-7xl text-white uppercase max-w-lg mx-auto">
          our
          <br />
          product
          <img src="/images/path.png" className="mt-3 lg:mt-10 mx-auto" />
        </h2>
        <div className="flex justify-center mt-5 xl:mt-16">
          <GridImageHome text="/images/logo-ultimatepay.jpg" />
          <GridImageHome text="/images/logo-ultimeal.jpg" duration={2500} />
        </div>
      </div>
    </section>
  );
}

export default ProductsHome;
