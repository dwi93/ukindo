import React from "react";
import Zoom from "react-reveal/Zoom";

function GridImageHome({ text, duration }) {
  return (
    <div className="w-1/3 xl:w-40 xl:h-40 overflow-hidden mx-auto mb-4 xl:mb-0 xl:mx-5">
      <Zoom duration={duration}>
      <img src={text} className="w-full img-hover transition-all duration-300" />
      </Zoom>
    </div>
  );
}

export default GridImageHome;
