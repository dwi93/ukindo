import React from "react";

function TitleSection({ title, color, spacing }) {
  return (
    <>
      <h3 className={`text-xl xl:text-2xl tracking-${spacing} font-sans font-semibold ${color} xl:mb-4 uppercase`}>
        {title}
      </h3>
    </>
  );
}

export default TitleSection;
