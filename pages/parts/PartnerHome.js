import React from "react";
import GridImageHome from "./GridImageHome";

function PartnerHome() {
  return (
    <section className="w-full home-partner">
      <div className="container w-full mx-auto mb-5 text-center px-4 xl:px-10 xl:py-20">
        <h2 className="font-sans font-semibold text-3xl xl:text-7xl text-white uppercase max-w-lg mx-auto">
          our
          <br />
          reliable
          <br className="hidden xl:block" />
          partner
          <img src="/images/path.png" className="mt-3 lg:mt-10 mx-auto" />
        </h2>

        <div className="flex justify-center mt-5 xl:mt-16">
          <GridImageHome text="/images/Takumi-1.png" />
          <GridImageHome text="/images/ARIT.png" duration={2500} />
        </div>
      </div>
    </section>
  );
}

export default PartnerHome;
