import { motion } from "framer-motion";
import Link from "next/link";
import React from "react";
import TitleSection from "./TitleSection";
import Particles from "react-tsparticles";

function Banner() {
  return (
    <div className="w-full py-20 lg:py-40 bg-header relative">
      <h3 className="text-white text-xl hidden lg:block absolute top-10 right-8">
        PT. Usaha Kreatif Indonesia
      </h3>
      <div className="z-10 relative container mx-auto my-40 flex h-full items-center px-4 xl:px-16 overflow-hidden">
        <div>
          <motion.h3
            initial={{ y: -1000 }}
            animate={{ y: 0 }}
            transition={{
              type: "tween",
              duration: "1",
            }}
            className="text-xs xl:text-2xl tracking-widest font-sans font-semibold text-gray-500 mb-4 uppercase"
          >
            ADVANCE AND PROFESSIONAL
          </motion.h3>
          <motion.h1
            initial={{ y: 1000 }}
            animate={{ y: 0 }}
            transition={{
              type: "tween",
              duration: "1",
            }}
            className="text-white font-sans font-bold text-5xl xl:text-9xl"
          >
            The Future <br />
            Starts Here.
          </motion.h1>
        </div>
      </div>
      <div className="about-home mt-10 lg:mt-96 z-10 relative" id="about">
        <div className="container mx-auto py-10 xl:py-16 px-4 xl:px-28 overflow-hidden">
          <TitleSection
            title="about us"
            color="text-gray-500"
            spacing="widest"
          />
          <h2 className="text-2xl xl:text-7xl text-white font-sans font-semibold max-w-lg">
            COMPANY <br className="hidden xl:block" />
            DESCRIPTION
            <img src="/images/path.png" className="mt-3 lg:mt-10" />
          </h2>
          <div className="flex justify-between relative items-center mt-5 xl:mt-10">
            <figure className="hidden lg:block w-full">
              <img src="/images/bg-about.png" alt="about" />
            </figure>
            <article className="xl:absolute xl:-bottom-12 xl:right-0 w-full xl:w-1/2 px-4 xl:px-8 py-5 xl:py-10 bg-indigo-900 border-lightBlue-300 border-l-8 border-b-8">
              <div className="flex justify-between items-center mb-3">
                <span className="font-sans font-semibold tracking-wider text-xs text-gray-500 uppercase">
                  about us
                </span>
                <span className="font-sans text-xl text-white font-light">
                  Our Background
                </span>
              </div>
              <h4 className="font-sans font-semibold text-2xl xl:text-3xl text-white">
                Usaha Kreatif Indonesia
              </h4>
              <div className="font-sans text-xl text-white my-3 xl:my-6">
                <p>
                  We are an IT-based company providing one stop solutions from
                  consultation to application development. Our team consists of
                  passionate, creative, highly qualified young soul
                  professionals who are ready to give you the best solution to
                  achieve your goals through technology.
                </p>
              </div>
              <Link href="/">
                <a className="font-sans text-xl text-cyan-300 flex items-center">
                  <span>Learn More</span>
                  <img
                    src="/images/icon-learn-more.png"
                    alt="learn-more"
                    className="w-12 xl:w-1/12"
                  />
                </a>
              </Link>
            </article>
          </div>
        </div>
      </div>
      <div className="absolute top-0 left-0 right-0 bottom-0">
        <Particles
          id="tsparticles"
          options={{            
            particles: {
              color: {
                value: "#ffffff",
              },
              links: {
                color: "#ffffff",
                distance: 150,
                enable: true,
                opacity: 0.5,
                width: 1,
              },
              collisions: {
                enable: true,
              },
              move: {
                direction: "none",
                enable: true,
                outMode: "bounce",
                random: false,
                speed: 6,
                straight: false,
              },
              number: {
                density: {
                  enable: true,
                  value_area: 800,
                },
                value: 35,
              },
              opacity: {
                value: 0.5,
              },
              shape: {
                type: "circle",
              },
              size: {
                random: true,
                value: 5,
              },
            },
            detectRetina: true,
          }}
          className="w-full h-full"
        />
      </div>
    </div>
  );
}

export default Banner;
