import emailjs from "emailjs-com";
import React, { useState } from "react";
import { toast, ToastContainer } from "react-nextjs-toast";

function FormMessage() {
  const [fields, setFields] = useState({
    name: "",
    phone: "",
    email: "",
    message: "",
  });

  const [status, setStatus] = useState("Send Message");

  const sendEmail = (e) => {
    e.preventDefault();

    setStatus("Loading...");
    emailjs
      .sendForm(
        "service_fohsuww",
        "template_2gpeogy",
        e.target,
        "user_3t8UuC9YUUhqoVlQS3cAF"
      )
      .then(
        () => {
          setStatus("Send Message");
          setFields({
            name: "",
            phone: "",
            email: "",
            message: "",
          });
          toast.notify("successful sending email", {
            duration: 5,
            type: "success",
          });
        },
        (error) => {
          setStatus("Send Message");
          toast.notify(error.text, {
            duration: 5,
            type: "success",
          });
          setFields({
            name: "",
            phone: "",
            email: "",
            message: "",
          });
        }
      );
  };

  const fieldsHandler = (e) => {
    const fieldName = e.target.getAttribute("name");

    setFields({
      ...fields,
      [fieldName]: e.target.value,
    });
  };

  return (
    <>
      <ToastContainer />
      <form onSubmit={sendEmail}>
        <div className="block xl:flex justify-between">
          <div className="form-control w-full xl:w-1/2 mb-4">
            <input
              type="text"
              name="name"
              className="w-full bg-indigo-900 px-5 py-5 border-transparent text-white placeholder-white rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent"
              placeholder="Name"
              value={fields.name}
              onChange={fieldsHandler.bind(this)}
              required
            />
            {/* <div className="text-red-500">{errors.name?.message}</div> */}
          </div>
          <div className="form-control w-full xl:w-1/2 mb-4 xl:ml-4">
            <input
              type="text"
              name="phone"
              className="w-full bg-indigo-900 px-5 py-5 border-transparent text-white placeholder-white rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent"
              placeholder="Phone Number"
              value={fields.phone}
              onChange={fieldsHandler.bind(this)}
              required
            />
            {/* <div className="text-red-500">{errors.phone?.message}</div> */}
          </div>
        </div>
        <div className="form-control w-full mb-4">
          <input
            type="email"
            name="email"
            className="w-full bg-indigo-900 px-5 py-5 border-transparent text-white placeholder-white rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent"
            placeholder="Email Address"
            value={fields.email}
            onChange={fieldsHandler.bind(this)}
            required
          />
          {/* <div className="text-red-500">{errors.email?.message}</div> */}
        </div>
        <div className="form-control w-full mb-4">
          <textarea
            rows="5"
            name="message"
            className="w-full bg-indigo-900 px-5 py-5 border-transparent text-white placeholder-white rounded-lg focus:outline-none focus:ring-2 focus:ring-indigo-900 focus:border-transparent"
            placeholder="Tell Us About Your Project...."
            value={fields.message}
            onChange={fieldsHandler.bind(this)}
            required
          ></textarea>
          {/* <div className="text-red-500">{errors.message?.message}</div> */}
        </div>
        <button
          className="bg-indigo-800 xl:bg-transparent text-white focus:outline-none rounded-full border-dotted border-4 border-indigo-900 px-3 py-3 w-full xl:w-1/4 flex justify-center items-center mx-auto hover:bg-indigo-800 transition-all duration-200 mt-4"
          type="submit"
        >
          {status}
        </button>
      </form>
    </>
  );
}

export default FormMessage;
