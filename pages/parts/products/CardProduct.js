import React from "react";

function CardProduct({ imgTitle, title }) {
  return (
    <>
      <div className="w-full overflow-hidden mx-auto shadow-md rounded-xl">
        <img src={`/images/${imgTitle}`} alt="ultimeal" className="w-full" />
      </div>
      <span className="text-lg font-semibold text-gray-600 tracking-wide mt-5 block">
        {title}
      </span>
    </>
  );
}

export default CardProduct;
