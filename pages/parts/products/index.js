import React from "react";
import Zoom from "react-reveal/Zoom";
import Fade from "react-reveal/Fade";
import CardProduct from "./CardProduct";

function Products() {
  return (
    <section className="bg-products">
      <div className="container px-4 mx-auto py-24 lg:py-16">
        <div className="flex justify-between items-center">
          <div className="w-full lg:w-1/2 pr-0 lg:pr-10">
            <h1 className="text-5xl xl:text-6xl text-center lg:text-left text-white font-bold">
              Our Product
            </h1>
            <p className="block text-lg text-center md:text-left text-gray-500 mt-3">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s.
            </p>
          </div>
          <div className="hidden lg:block w-1/2">
            {/* <Zoom>
              <img
                src="/images/product_img.png"
                alt="product"
                className="w-3/4 mx-auto"
              />
            </Zoom> */}
            <h2 className="text-5xl text-white font-bold">Coming Soon</h2>
          </div>
        </div>
        {/* <div className="w-full block lg:flex justify-center items-center text-center mt-10">
          <Fade duration={3000}>
            <div className="flex-col justify-center w-full md:w-1/2 lg:w-2/12 p-5 rounded-2xl mx-auto lg:mx-5 mt-5 mg:mt-0 shadow-xl">
              <CardProduct imgTitle="logo-ultimeal.jpg" title="Ultimeal" />
            </div>
          </Fade>
          <Fade duration={3500}>
            <div className="flex-col justify-center w-full md:w-1/2 lg:w-2/12 p-5 rounded-2xl mx-auto lg:mx-5 mt-5 mg:mt-0 shadow-xl">
              <CardProduct
                imgTitle="logo-ultimatepay.jpg"
                title="Ultimatepay"
              />
            </div>
          </Fade>
        </div> */}
      </div>
    </section>
  );
}

export default Products;
