import Layout from "components/Layout";
import OurService from "./parts/ourservice/OurService";

export default function OurServices() {
  return (
    <Layout title="Services">
      <main>
        <OurService />
      </main>
    </Layout>
  )
}