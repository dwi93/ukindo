import Layout from "components/Layout";
import React from "react";
import Products from "./parts/products";

function Product() {
  return (
    <Layout title="Products">
      <main>
        <Products />
      </main>
    </Layout>
  );
}

export default Product;
