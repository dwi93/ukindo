import Layout from "components/Layout";
import ContentHome from "./parts/ContentHome";
import Header from "./parts/Header";
import ServiceHome from "./parts/ServiceHome";

export default function Home() {
  return (
    <Layout title="">
      <main>
        <Header />
        <ServiceHome />
        <ContentHome />
      </main>
    </Layout>
  );
}
