import Layout from "components/Layout";
import React from "react";
import Hero from "./parts/about/Hero";
import OurValue from "./parts/about/OurValue";

function AboutUs() {
  return (
    <Layout title="About">
      <main>
        <Hero />
        <OurValue />
      </main>
    </Layout>
  );
}

export default AboutUs;
