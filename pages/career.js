import Layout from "components/Layout";
import React from "react";
import Career from "./parts/career/Career";

function career() {
  return (
    <Layout title="Career">
      <main>
        <div className="bg-career">
          <Career />
        </div>
      </main>
    </Layout>
  );
}

export default career;
