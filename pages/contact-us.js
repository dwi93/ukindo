import Layout from "components/Layout";
import React from "react";
import Contact from "./parts/contact/Contact";

function ContactUs() {
  return (
    <Layout title="Contact">
      <main>
        <div className="bg-contact pt-24 lg:pt-40">
          <Contact />
        </div>
      </main>
    </Layout>
  );
}

export default ContactUs;
