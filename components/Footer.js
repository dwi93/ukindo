import Link from "next/link";
import React, { useEffect } from "react";
import Address from "./Address";

const mapboxgl = require("mapbox-gl/dist/mapbox-gl.js");

mapboxgl.accessToken =
  "pk.eyJ1IjoiYW5kcmVkd2k5MyIsImEiOiJja2llY280YmUwOHI4MnVwMTh5dnQ3dmpiIn0.00EbWDM0qKD5YUx-TrKUFg";

function Footer() {
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: "my-map",
      style: "mapbox://styles/mapbox/streets-v11",
      center: [112.739727, -7.261647],
      zoom: 15,
    });
    map.on("load", () => {
      new mapboxgl.Marker().setLngLat([112.739727, -7.261647]).addTo(map);
    });
    map.scrollZoom.disable();
  }, []);
  return (
    <footer>
      <div className="container mx-auto py-10 xl:py-16 px-4">
        <div className="w-full lg:w-1/2 mx-auto">
          <img src="/images/logo-type.png" alt="logo-footer" />
        </div>
        <div className="hidden xl:flex justify-between items-center px-60">
          <Link href="/">
            <a className="link text-base text-gray-500 font-semibold hover:text-white uppercase tracking-widest mx-5 ml-0">
              home
            </a>
          </Link>
          <Link href="/about-us">
            <a className="link text-base text-gray-500 font-semibold hover:text-white uppercase tracking-widest mx-5">
              about us
            </a>
          </Link>
          <Link href="/our-services">
            <a className="link text-base text-gray-500 font-semibold hover:text-white uppercase tracking-widest mx-5">
              service
            </a>
          </Link>
          <Link href="/product">
            <a className="link text-base text-gray-500 font-semibold hover:text-white uppercase tracking-widest mx-5">
              product
            </a>
          </Link>
          <Link href="/career">
            <a className="link text-base text-gray-500 font-semibold hover:text-white uppercase tracking-widest mx-5">
              career
            </a>
          </Link>
          <Link href="/contact-us">
            <a className="link text-base text-gray-500 font-semibold hover:text-white uppercase tracking-widest mx-5 mr-0">
              contact us
            </a>
          </Link>
        </div>
        <img src="/images/highlight-footer.png" className="mb-8 lg:mb-0" />
        <div className="block text-center">
          <div className="w-full">
            <Address />
          </div>
          <div className="w-2/3 lg:w-1/3 mt-3 lg:mt-5 mx-auto">
            <div className="xl:mx-5">
              <h5 className="text-base text-indigo-800 font-semibold uppercase tracking-widest mb-2">
                our location
              </h5>
              <div className="relative w-full overflow-hidden my-map">
                <div
                  id="my-map"
                  className="absolute w-full h-full bottom-0 right-0"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="w-full text-center py-5 bg-gray-200 text-gray-500 text-xs">
        <p>Copyright &copy; PT. Usaha Kreatif Indonesia</p>
      </div>
    </footer>
  );
}

export default Footer;
