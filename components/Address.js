import React from "react";

function Address({color}) {
  return (
    <>
      <h5 className={`text-base ${color == null ? "text-indigo-800" : color} font-bold uppercase tracking-widest mb-2`}>
        our office
      </h5>
      <address className="not-italic text-gray-500 text-lg">
        Pakuwon Center Lt 16 no.05, Kedungdoro, Tegalsari, <br className="hidden xl:block" />
        Surabaya City, East Java 60251
      </address>
      <div className="mt-5">
        <h5 className={`text-base ${color == null ? "text-indigo-800" : color} font-bold uppercase tracking-widest mb-2`}>
          our phone
        </h5>
        <span className="text-gray-500 text-lg">031 992 539 68 (Office)</span>
      </div>
      <div className="mt-5">
        <h5 className={`text-base ${color == null ? "text-indigo-800" : color} font-bold uppercase tracking-widest mb-2`}>
          our email
        </h5>
        <span className="text-gray-500 text-lg">info@usahakreatif.co.id</span>
      </div>
    </>
  );
}

export default Address;
