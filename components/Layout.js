import Navbar from "./Navbar";
import Footer from "./Footer";
import ScrollTop from "./ScrollTop";
import Head from "next/head";

function Layout(props) {
  return (
    <>
      <Head>
        <title>{props.title !== "" ? `${props.title} - ` : ""}PT. Usaha Kreatif Indonesia</title>
        <link rel="icon" href="/images/icon.png" />
        <link
          href="https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css"
          rel="stylesheet"
        />
      </Head>
      <Navbar />
      {props.children}
      <Footer />
      <ScrollTop />
    </>
  );
}

export default Layout;
