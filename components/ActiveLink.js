import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

function ActiveLink({ children, href }) {
  const router = useRouter();

  return (
    <Link href={href}>
      <a
        className={`font-medium text-xl lg:text-7xl mb-5 lg:mb-8 ${
          router.pathname === href ? "text-cyan-400" : "text-white"
        }  hover:text-cyan-400 block`}
      >
        {children}
      </a>
    </Link>
  );
}

export default ActiveLink;
