import Link from "next/link";
import React, { useState } from "react";
import ActiveLink from "./ActiveLink";

function Navbar() {
  const [toggleMenu, setToggleMenu] = useState(false);
  return (
    <header>
      <div className="fixed top-10 left-4 lg:left-8 h-auto lg:h-full z-40">
        <Link href="/">
          <a className="w-1/4 lg:w-1/2 block bg-black bg-opacity-5 shadow-xl rounded-md">
            <img
              src="/images/logo-uki-putih.png"
              alt="logo"
              className="w-full"
            />
          </a>
        </Link>
        <div className="absolute top-1/2 left-0 hidden xl:block">
          <a href="https://www.linkedin.com/company/usahakreatif" target="_blank" className="w-1/3 block ml-3 mb-3 bg-black bg-opacity-10 rounded-lg shadow-md">
            <img src="/images/in-1.png" alt="linkedin" className="w-full" />
          </a>
          <a href="https://www.instagram.com/usahakreatifindo/" target="_blank" className="w-1/3 block ml-3 mb-3 bg-black bg-opacity-10 rounded-lg shadow-md">
            <img src="/images/ig-1.png" alt="instagram" className="w-full" />
          </a>
          <a href="https://www.facebook.com/usahakreatifindo/" target="_blank" className="w-1/4 block p-1 ml-3 mb-3 bg-black bg-opacity-10 rounded-lg shadow-md">
            <img src="/images/fb-1.png" alt="facebook" className="w-full" />
          </a>
        </div>
      </div>
      <div className="fixed z-50 top-10 right-4 lg:top-1/2 lg:right-8">
        <button
          onClick={() => setToggleMenu((prev) => !prev)}
          className={["toggle z-50", toggleMenu ? "active" : ""].join(" ")}
        >
          <span></span>
          <span></span>
          <span></span>
        </button>
      </div>
      <div className="container block">
        <nav
          className={[
            "w-full z-40",
            toggleMenu ? "show-menu" : "hide-menu",
          ].join(" ")}
        >
          <div className="absolute lg:top-36 lg:right-60 top-10 right-0 w-full lg:w-auto pl-5 lg:pl-0">
            <ActiveLink href="/">Home</ActiveLink>
            <ActiveLink href="/about-us">About Us</ActiveLink>
            <ActiveLink href="/our-services">Service</ActiveLink>
            <ActiveLink href="/product">Product</ActiveLink>
            <ActiveLink href="/career">Career</ActiveLink>
            <ActiveLink href="/contact-us">Contact Us</ActiveLink>

            <div className="mt-5 flex items-center">
              <a href="https://www.linkedin.com/company/usahakreatif" target="_blank" className="w-2/12 lg:w-1/4 inline-block overflow-hidden">
                <img src="/images/in.png" alt="linkedin" className="w-full" />
              </a>
              <a href="https://www.instagram.com/usahakreatifindo/" target="_blank" className="w-2/12 lg:w-1/4 inline-block overflow-hidden">
                <img src="/images/ig.png" alt="instagram" className="w-full" />
              </a>
              <a href="https://www.facebook.com/usahakreatifindo/" target="_blank" className="w-2/12 lg:w-1/4 inline-block overflow-hidden">
                <img src="/images/fb.png" alt="facebook" className="w-full" />
              </a>
            </div>
          </div>
        </nav>
      </div>
    </header>
  );
}

export default Navbar;
