import { useEffect, useState } from "react";

function ScrollTop() {
  const [scroll, setScroll] = useState(0);

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.onscroll = () => {
        let currentScrollPos = window.pageYOffset;
        // let maxScroll = document.body.scrollHeight - window.innerHeight;
        if (currentScrollPos > 300) {
          setScroll(1);
        } else {
          setScroll(0);
        }
      };
    }
  }, []);

  const toTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <button
      onClick={toTop}
      className={`focus:outline-none scroll-top fixed bottom-10 right-6 transition-all duration-200 opacity-${scroll}`}
    >
      <img
        src="/images/arrow-up.png"
        alt="arrow up"
        className="w-full transform -rotate-45"
      />
    </button>
  );
}

export default ScrollTop;
